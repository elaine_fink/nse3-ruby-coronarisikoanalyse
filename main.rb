class Entry
  def initialize(bm,vk)
    @bmi = bm
    @vork = vk
  end
  def bearbeiten(bm,vk)
    @bmi =bm
    @vork = vk
  end
  def printbmi
    @bmi
  end
  def printvork
    @vork
  end
end

def bmi_berechnen(entrys)
  puts ""
  puts ""
  puts "BERECHNUNG DES BMIS UND DES CORONA RISIKOS"
  puts ""
  print "Bitte geben Sie Ihre Größe in Meter ein: "
  groesse = gets.chomp()
  print "Bitte geben Sie Ihr Gewicht in Kilogramm ein: "
  gewicht = gets.chomp()
  bmi =gewicht.to_f/(groesse.to_f**2)
  puts""
  puts ("Ihr BMI beträgt " + String(bmi))
  puts ""
  z = true
  while z
    print ("Leiden Sie bereits an einer oder mehreren Vorerkrankungen? (ja/nein) ")
    vork = gets.chomp()
    if vork == "ja" or vork == "nein"
      z=false
      risiko_berechnen(bmi,vork)
      e = Entry.new(bmi,vork)
      entrys.push(e);
      b = e.printbmi()
      v = e.printvork()
      puts ("BMI: " + String(b)+"  -  Vorerkrankungen: " + String(v))
    else
      puts("Bitte geben Sie einen gültigen Wert ein!")
    end
  end
end

def risiko_berechnen(bmi,vork)
  puts""
  if vork == "nein" && 18.4<bmi && bmi<=24.9
    puts("Sie haben ein niedriges Risko!")
  elsif vork == "nein" && 24.9<bmi && bmi<=34.9 or vork == "nein" && 16.0<bmi && bmi<=18.4
    puts("Sie haben ein mittleres Risiko!")
  elsif vork == "ja" && 18.4<bmi && bmi<=24.9
    puts("Sie haben ein mittleres bis hohes Risko!")
  elsif vork == "ja" && 24.9<bmi && bmi<=34.9 or vork == "ja" && 16.0<bmi && bmi<=18.4 or vork == "nein" && 34.9<=bmi or vork == "nein" && bmi<16.0
    puts("Sie habne ein hohes Risiko!")
  elsif vork == "ja" and  34.9<=bmi or vork == "ja" and bmi<16.0
    puts("Sie haben ein sehr hohes Risiko!")
  end
  puts ""
end

def tipps()
  puts ""
  puts ""
  puts "TIPPS ZUR VERMINDERUNG DES CORONA RISIKOS"
  puts ""
  puts "- Ernähren Sie sich gesund!"
  puts "- Betreiben Sie Sport!"
  puts "- Halten Sie Abstand!"
  puts "- Tragen Sie eine FFP2 Maske in öffendlichen, geschlossenen Räumen!"
  puts "- Reduzieren Sie Ihre sozialen Kontakte!"
  puts ""
end

def liste(entrys)
  puts ""
  puts ""
  puts "LISTE DER BISHERIGEN EINGABEN"
  puts ""
  if entrys.length == 0
    puts "Bisher gab es keine Eingaben"
    puts "Wählen Sie beim Menü '1' aus und geben Sie Ihre Daten ein"
  else
    i =0
    for e in entrys
      b = e.printbmi()
      v = e.printvork()
      puts ( String(i)+ "  -  BMI: " + String(b)+"  -  Vorerkrankungen: " + String(v))
      i = i+1
    end
    puts ""
    print"Wollen Sie einen der Datensätze bearbeiten? (ja/nein) "
    n = gets.chomp()
    puts ""
    if (n == "ja")
        eintrag_bearbeiten(entrys)
    end
  end
end

def eintrag_bearbeiten(entrys)
  print "Bitte geben Sie die Nummer des Datensatzes an (0,1,2 ...): "
  m = gets.chomp().to_i
  puts ""
  i =0
  for e in entrys
    if m == i
      b = e.printbmi()
      v = e.printvork()
      puts ( String(i)+ "  -  BMI: " + String(b)+"  -  Vorerkrankungen: " + String(v))
      print "Bitte geben Sie die neue Größe in Meter ein: "
      groesse = gets.chomp()
      print "Bitte geben Sie das neue Gwicht in Kilogramm ein: "
      gewicht = gets.chomp()
      bmi =gewicht.to_f/(groesse.to_f**2)
      puts""
      puts ("Ihr neuer BMI beträgt " + String(bmi))
      puts ""
      z = true
      while z
        print ("Leiden Sie bereits an Vorerkrankungen? (ja/nein) ")
        vork = gets.chomp()
        if vork == "ja" or vork == "nein"
          z=false
          risiko_berechnen(bmi,vork)
          e.bearbeiten(bmi,vork)
          b = e.printbmi()
          v = e.printvork()
          puts ("BMI: " + String(b)+"  -  Vorerkrankungen: " + String(v))
        else
          puts("Bitte geben Sie einen gültigen Wert ein!")
        end
      end
    end
    i = i+1
  end
end

puts ""
puts ""
puts ("Willkomen bei Corona-Risko-Analyse!")
puts ""
puts ""
print ("Bitte geben Sie Ihren Namen ein: ")
entrys = Array.new
name = gets.chomp()
puts ("Hallo " + name + "!")
condition = true
while condition ==true
  puts ""
  puts ("Wenn Sie Ihren BMI und damit Ihr Corona Risko berechnen wollen, geben Sie 1 ein.")
  puts ("Wenn Sie Tipps zur Verminderung Ihres Corona-Riskos haben wollen, geben Sie 2 ein.")
  puts ("Wenn Sie Ihre bisherigen Eingaben sehen/bearbeiten wollen, geben Sie 3 ein.")
  print ("Bitte geben Sie die Zahl ihrer Wahl ein: ")
  x = gets.chomp()
  if x == "1"
    bmi_berechnen(entrys)
  elsif x == "2"
    tipps()
  elsif x == "3"
    liste(entrys)
  else
    puts("Bitte geben Sie einen gültigen Wert ein!")
  end
  puts ""
  puts ""
  print "Wollen Sie das Programm beenden? (ja/nein) "
  y = gets.chomp()
  if y == "ja"
     condition = false
      puts ""
    puts "Danke für Ihre Teilnahme"
   end
    
end
